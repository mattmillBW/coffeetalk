package com.brightwolf.coffeetalk.service;

import java.util.EnumSet;

import com.brightwolf.coffeetalk.api.CoffeeRequest;
import com.brightwolf.coffeetalk.api.CoffeeResponse;
import com.brightwolf.coffeetalk.api.CupType;
import com.brightwolf.coffeetalk.api.DrinkType;
import com.brightwolf.coffeetalk.db.CoffeeTalkDb;
import com.brightwolf.coffeetalk.db.CupTypeServiceRemote;
import com.brightwolf.coffeetalk.db.model.User;

public class CoffeeTalk {
    private final CoffeeTalkDb db;
    private final CupTypeServiceRemote cupTypeService;
    
    private final String DEFAULT_NAME = "valued customer";
    private final EnumSet<DrinkType> validDrinkTypes = EnumSet.of(DrinkType.BLACK_COFFEE, DrinkType.WATER);
    
    public CoffeeTalk(CoffeeTalkDb db, CupTypeServiceRemote cupTypeService) {
        this.db = db;
        this.cupTypeService = cupTypeService;
    }
    
    public CoffeeResponse handleRequest(CoffeeRequest request) {
        var name = resolveNameFromDbOrDefault(request);
        var drink = validateRequest(request);
        var cup = getCupType(drink, request.getAmount());
        return CoffeeResponse.builder()
            .name(name)
            .drink(drink)
            .cup(cup)
            .build();
    }
    
    private DrinkType validateRequest(CoffeeRequest request) {
        return db.getDrinkTypeForRequest(request.getDrinkType())
            .filter(validDrinkTypes::contains)
            .orElseThrow(() -> new CoffeeRequestInvalidException("You must provide a valid drink type"));
    }
    
    private String resolveNameFromDbOrDefault(CoffeeRequest request) {
        return db.getUserFromUserId(request.getUserId())
            .map(User::getName)
            .orElse(DEFAULT_NAME);
    }
    private CupType getCupType(DrinkType drink, Double amount) {
        return db.getCupTypeForDrinkTypeAndSize(drink, amount)
            .orElseThrow(() -> new CoffeeRequestInvalidException("Could not find a valid cup type"));
    }
    
}
