package com.brightwolf.coffeetalk.api;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum DrinkType {
    BLACK_COFFEE(Boolean.FALSE, BigDecimal.valueOf(0.08)),
    LATTE(Boolean.TRUE, BigDecimal.valueOf((0.17))),
    WATER(null, BigDecimal.valueOf(0));
    
    private final Boolean espresso;
    private final BigDecimal pricePerOz;
}
