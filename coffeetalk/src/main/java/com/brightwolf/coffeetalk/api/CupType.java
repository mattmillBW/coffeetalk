package com.brightwolf.coffeetalk.api;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum CupType {
    STANDARD_CUP(16.5),
    LARGE_CUP(24.0),
    CARAFE(180.0),
    ESPRESSO(4.0);
    
    Double volume;
}
