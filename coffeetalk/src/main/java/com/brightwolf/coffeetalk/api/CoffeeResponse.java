package com.brightwolf.coffeetalk.api;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CoffeeResponse {
    String name;
    DrinkType drink;
    CupType cup;
}
