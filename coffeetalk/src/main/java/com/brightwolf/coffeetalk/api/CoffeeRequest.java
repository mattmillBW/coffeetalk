package com.brightwolf.coffeetalk.api;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CoffeeRequest {
    String userId;
    String drinkType;
    Double amount;
}