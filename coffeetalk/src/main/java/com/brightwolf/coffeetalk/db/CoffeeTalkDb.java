package com.brightwolf.coffeetalk.db;

import java.util.Map;
import java.util.Optional;

import com.brightwolf.coffeetalk.api.CupType;
import com.brightwolf.coffeetalk.api.DrinkType;
import com.brightwolf.coffeetalk.db.model.User;

public class CoffeeTalkDb {
    private Map<String, User> userIdToNameRepo;
    
    public CoffeeTalkDb(Map<String, User> userIdToNameRepo) {
        this.userIdToNameRepo = userIdToNameRepo;
    }
    
    public Optional<User> getUserFromUserId(String userId) {
        return Optional.ofNullable(userIdToNameRepo.get(userId));
    }
    
    public Optional<DrinkType> getDrinkTypeForRequest(String requestType) {
        try {
            return Optional.of(DrinkType.valueOf(requestType));
        } catch (Exception e) {
            return Optional.empty();
        }
    }
    
    public Optional<CupType> getCupTypeForDrinkTypeAndSize(DrinkType drink, Double amount) {
        if(amount < 16) {
            return Optional.of(CupType.STANDARD_CUP);
        } else {
            return Optional.empty();
        }
        
    }
}
