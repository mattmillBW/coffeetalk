package com.brightwolf.coffeetalk.db.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class User {
    String userId;
    String name;
}
