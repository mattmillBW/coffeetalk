package com.brightwolf;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Map;
import java.util.concurrent.TimeUnit;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.brightwolf.coffeetalk.api.CoffeeRequest;
import com.brightwolf.coffeetalk.db.CoffeeTalkDb;
import com.brightwolf.coffeetalk.db.CupTypeServiceRemote;
import com.brightwolf.coffeetalk.db.model.User;
import com.brightwolf.coffeetalk.service.CoffeeRequestInvalidException;
import com.brightwolf.coffeetalk.service.CoffeeTalk;

class CoffeeTalkTest {
    CoffeeTalk service;
    
    @BeforeEach
    void setup(){
        var db = new CoffeeTalkDb(Map.of(
            "1", User.builder().name("Matt").build(),
            "2", User.builder().build()
        ));
        var cupTypeService = new CupTypeServiceRemote();
        service = new CoffeeTalk(db, cupTypeService);
    }
    
    @Test
    void testCupType_runsFast() {
        Instant start = Instant.now();
        service.handleRequest(CoffeeRequest.builder()
            .userId("1")
            .drinkType("BLACK_COFFEE")
            .amount(8.0)
            .build());
        Instant end = Instant.now();
        assertTrue(Duration.between(end, start).getSeconds() < 4);
    }
    
    @Test
    void testCupType_runsSlow() {
        Instant start = Instant.now();
        service.handleRequest(CoffeeRequest.builder()
            .userId("1")
            .drinkType("BLACK_COFFEE")
            .amount(24.0)
            .build());
        Instant end = Instant.now();
        assertTrue(Duration.between(start, end).getSeconds() > 4);
    }
    
    
    @Test
    void testDrinkType_throwsExceptionOnEmptyRequest() {
        assertThrows(CoffeeRequestInvalidException.class, () -> service.handleRequest(
            CoffeeRequest.builder()
                .userId("1")
                .build())
        );
    }
    
    @Test
    void testDrinkType_throwsExceptionOnUnknownDrinkType() {
        assertThrows(CoffeeRequestInvalidException.class, () -> service.handleRequest(
            CoffeeRequest.builder()
                .userId("1")
                .drinkType("UNKNOWN")
                .build())
        );
    }
    
    @Test
    void testDrinkType_throwsExceptionOnUnavailableDrinkType() {
        assertThrows(CoffeeRequestInvalidException.class, () -> service.handleRequest(
            CoffeeRequest.builder()
                .userId("1")
                .drinkType("LATTE")
                .build())
        );
    }
    
    @Test
    void testUserName_presentWhenUserFound() {
        var response = service.handleRequest(
            CoffeeRequest
                .builder()
                .userId("1")
                .drinkType("BLACK_COFFEE")
                .build());
    
        assertThat(response.getName(), is("Matt"));
    }
    
    @Test
    void testUserName_defaultWhenUserNameNull() {
        var response = service.handleRequest(
            CoffeeRequest
                .builder()
                .userId("2")
                .drinkType("BLACK_COFFEE")
                .build());
        
        assertThat(response.getName(), is("valued customer"));
    }
    
    @Test
    void testUserName_defaultWhenUserNotFound() {
        var response = service.handleRequest(
            CoffeeRequest
                .builder()
                .userId("3")
                .drinkType("BLACK_COFFEE")
                .build());
        
        assertThat(response.getName(), is("valued customer"));
    }
}